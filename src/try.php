<?php
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);

require "../vendor/autoload.php";
require "classes.php";

$latte = new Latte\Engine;
$latte->setTempDirectory('../temp');

$User = new User();
$User -> nacitaniDatabaze();

$dataUzivatele = ['user' => $User,];
$latte->render('../templates/homePage.latte', $dataUzivatele);
?>
