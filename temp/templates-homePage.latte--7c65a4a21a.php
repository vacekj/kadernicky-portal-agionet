<?php

use Latte\Runtime as LR;

/** source: ../templates/homePage.latte */
final class Template7c65a4a21a extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>';
		echo LR\Filters::escapeHtmlText($user->getUserName()) /* line 10 */;
		echo '</h1>



</body>
</html>';
		return get_defined_vars();
	}

}
